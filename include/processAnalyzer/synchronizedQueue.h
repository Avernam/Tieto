#ifndef PROC_ANALYZER_PROC_SYNCHRONIZED_QUEUE_H
#define PROC_ANALYZER_PROC_SYNCHRONIZED_QUEUE_H

/*
Current implementation assumes no end condition.
i.e. queue can not be gracefully killed while there is anyone waiting
*/

// Forward declaration
struct SynchronizedQueue;

// Function to initialize SynchronizedQueue.
// Allocated SynchronizedQueue should be destroyed with destroySynchronizedQueue()
// Returns pointer to initialized SynchronizedQueue
struct SynchronizedQueue* createSynchronizedQueue();

// Function to shutdown previously created SynchronizedQueue.
// It still needs to be destroyed!
void shutdownSynchronizedQueue(struct SynchronizedQueue* queue);

// Function to destroy previously created SynchronizedQueue.
void destroySynchronizedQueue(struct SynchronizedQueue* queue);

// Function to put data in SynchronizedQueue.
// In case of success control over userData is handed over to queue.
// "id" is intended as information for consumer only and is returned when element is popped out from queue.
// Returns 0 on success
// Returns < 0 on fail
int pushToSynchronizedQueue(struct SynchronizedQueue* queue, unsigned short id, void* userData);
#define PUSH_TO_SYNCHRONIZED_QUEUE_INVALID_VALUE -1 // Returned when e.g. queue == NULL
#define PUSH_TO_SYNCHRONIZED_QUEUE_SYNC_ERROR    -2 // Returned when we fail to lock on synchronization
#define PUSH_TO_SYNCHRONIZED_QUEUE_SHUTDOWN      -3 // Returned when queue is in shutdown state

// Function to get data from SynchronizedQueue.
// In case of success control over userData is handed over to caller.
// "id" is intended as information for consumer
// Returns 0 on success
// Returns < 0 on fail
int popFromSynchronizedQueue(struct SynchronizedQueue* queue, unsigned short* id, void** userData);
#define POP_FROM_SYNCHRONIZED_QUEUE_INVALID_VALUE    -1 // Returned when e.g. queue == NULL
#define POP_FROM_SYNCHRONIZED_QUEUE_SYNC_ERROR       -2 // Returned when we fail to lock on synchronization
#define POP_FROM_SYNCHRONIZED_QUEUE_EMPTY_AFTER_SYNC -3 // Returned when we did get sync but queue was empty at a time
#define POP_FROM_SYNCHRONIZED_QUEUE_SHUTDOWN         -4 // Returned when queue is in shutdown state

#endif
