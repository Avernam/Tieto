#ifndef PROC_ANALYZER_PROC_STAT_DATA_H
#define PROC_ANALYZER_PROC_STAT_DATA_H

// Structure designed to hold CPU load data as stored in /proc/stat
typedef struct CpuLoad
{
  // This holds data how much time CPU was used by user-space to this point in time
  unsigned long user;
  // This holds data how much time CPU was used by user-space in low-priority to this point in time
  unsigned long nice;
  // This holds data how much time CPU was used by system to this point in time
  unsigned long system;
  // This holds data how much time CPU was idle to this point in time
  unsigned long idle;
  // This holds data how much time CPU was spent waiting for I/O to this point in time
  unsigned long ioWait;
  // This holds data how much time CPU was spent when servicing interrupts to this point in time
  unsigned long irq;
  // This holds data how much time CPU was spent when servicing soft-interrupts to this point in time
  unsigned long softIrq;
  // This holds data how much time CPU was spent 'stolen' by other OSes to this point in time
  unsigned long steal;
  // This holds data how much time CPU was spent servicing virtual CPU for guest OS to this point in time
  unsigned long guest;
  // This holds data how much time CPU was spent servicing virtual CPU for guest OS in low-priority to this point in time
  unsigned long guestNice;
} CpuLoad;

// Structure designed to hold CPU Load data for all CPUs in the system
typedef struct ProcStatData
{
  // Number of CPUs as returned by sysinfo get_nprocs()
  unsigned short cpuCount;
  // Aggregated CPU Load for all CPUs in the system
  CpuLoad aggregated;
  // Placeholder array for CPU Load data per-CPU in the system - use FAM (Flexible Array Member)
  CpuLoad cpu[];
} ProcStatData;

#endif
