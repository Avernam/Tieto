#ifndef PROC_ANALYZER_READER_H
#define PROC_ANALYZER_READER_H

struct SynchronizedQueue;

typedef struct ReaderContext
{
  // A Queue where we will put our data after each read operation
  struct SynchronizedQueue* outQueue;
  // How often should we perform reader loop
  unsigned int readIntervalMs;
} ReaderContext;

// Function meant to be used with thrd_create. Do not use it as standalone function!
// Input is context described as above
// Thread completes autoatically on SIGINT and SIGTERM signals or when error occurs.
int reader(void* context);

#endif
