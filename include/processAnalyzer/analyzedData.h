#ifndef PROC_ANALYZER_ANALYZED_DATA_H
#define PROC_ANALYZER_ANALYZED_DATA_H

typedef struct CpuLoadRelative
{
  // This holds data how much of CPU is used by user-space (Guest usage not counted)
  unsigned long user;
  // This holds data how much of CPU is used by user-space in low-priority  (Guest usage not counted)
  unsigned long nice;
  // This holds data how much of CPU is used by system
  unsigned long system;
  // This holds data how much of CPU is idle
  unsigned long idle;
  // This holds data how much of CPU is used by guest OS
  unsigned long guest;
  // This holds data how much of CPU is used by guest OS in low-priority
  unsigned long guestNice;
  // This holds data how much of CPU is busy in overall
  unsigned long busy;
} CpuLoadRelative;

typedef struct AnalyzedData
{
  // Number of CPUs aas read from ProcStatData
  unsigned short cpuCount;

  // Precision of the data (number of numbers after dot) in CpuLoadRelative
  unsigned short precision;

  // Averaged CPU Load for all CPUs in the system
  CpuLoadRelative average;

  // Per-CPU statistics placeholder - use FAM (Flexible Array Member)
  CpuLoadRelative cpu[];
} AnalyzedData;

#endif
