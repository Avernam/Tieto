#ifndef PROC_ANALYZER_PROC_STAT_DATA_ANALYZER_H
#define PROC_ANALYZER_PROC_STAT_DATA_ANALYZER_H

// Forward Declarations
struct ProcStatData;
struct AnalyzedData;

// Function analyzing parsed procStat from "/proc/stat". Pointer to analyzed data is put in structure under *data.
// The function calculates the delta between Current and Prev to get usage between these 2 data points.
// Both ProcStatData and AnalyzedData are managed by caller
// Return = 0 on success. From this point caller is responsible for freeing memory under *data.
// Return < 0 on error.
int analyzeProcStatData(unsigned short precision,
                        struct ProcStatData* procStatPrev,
                        struct ProcStatData* procStatCurrent,
                        struct AnalyzedData** data);
#define ANALYZE_PROC_STAT_DATA_INVALID_ARGUMENT -1 // Returned when either procStat or data are NULL or malformed

#endif
