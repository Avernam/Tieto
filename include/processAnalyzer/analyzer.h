#ifndef PROC_ANALYZER_ANALYZER_H
#define PROC_ANALYZER_ANALYZER_H

// Forward Declaration
struct SynchronizedQueue;

typedef struct AnalyzerContext
{
  // This queue is shared with Reader thread and it's a place where we receive data for analysis
  struct SynchronizedQueue* inQueue;
  // This queue is shared with Printer thread and it's a place where we send data after analysis
  struct SynchronizedQueue* outQueue;
  // Configured precision for analyzed data (numbers after dot)
  unsigned short precision;
} AnalyzerContext;

int analyzer(void* context);

#endif
