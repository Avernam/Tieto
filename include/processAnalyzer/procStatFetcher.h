#ifndef PROC_ANALYZER_PROC_STAT_FETCHER_H
#define PROC_ANALYZER_PROC_STAT_FETCHER_H

// Forward declaration
struct ProcStatData;

// Method to retreive CPU Load
// Return = 0 on success. In this scenario caller is responsible for freeing memory pointed by *cpuLoad.
// Return < 0 on failure. In this scenario *cpuLoad doesn't need to be freed.
int fetchCpuLoad(struct ProcStatData** cpuLoad);
#define FETCH_CPU_LOAD_INVALID_VALUE         -1 // Returned when e.g. cpuLoad == NULL
#define FETCH_CPU_LOAD_FAILED_TO_OPEN_SOURCE -2 // Returned when /proc/stat couldn't be opened
#define FETCH_CPU_LOAD_FAILED_TO_READ_SOURCE -3 // Returned when /proc/stat couldn't be read from
#define FETCH_CPU_LOAD_MALFORMED_SOURCE_DATA -4 // Returned when /proc/stat returned unexpected data

#endif
