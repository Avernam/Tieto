#ifndef PROC_ANALYZER_PRINTER_H
#define PROC_ANALYZER_PRINTER_H

// Forward Declaration
struct SynchronizedQueue;

typedef struct PrinterContext
{
  // This queue is shared with Analyzer thread and it's a place where we receive data to print
  struct SynchronizedQueue* inQueue;
  // Configured precision for analyzed data (numbers after dot)
  unsigned short precision;
} PrinterContext;

int printer(void* context);

#endif
