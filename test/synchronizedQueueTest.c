#include <assert.h> // Defines assert()
#include <stdio.h>  // Defines printf()
#include <threads.h>

// Include Code under Test
#include "processAnalyzer/synchronizedQueue.h"
#include "synchronizedQueue.c"

// Test if SynchronizedQueue gets created properly
void test0_createSynchronizedQueue()
{
  // Log that we started this testcase
  printf("BEGIN: test0_createSynchronizedQueue\n");

  // Create the queue
  SynchronizedQueue* queue = createSynchronizedQueue();

  // Fail if queue is NULL
  assert(queue != NULL);

  // Fail if queue first element is not NULL after creation
  assert(queue->first == NULL);

  // Fail if queue last element is not NULL after creation
  assert(queue->last == NULL);

  // Clean up concurrency guards
  mtx_destroy(&queue->concurrencyMutex);
  sem_destroy(&queue->queueFillSemaphore);

  // Free the queue
  free(queue);

  // Log that we passed this testcase
  printf("END: test0_createSynchronizedQueue\n");
}

// Test if SynchronizedQueue gets destroyed properly
void test0_destroySynchronizedQueue()
{
  // Log that we started this testcase
  printf("BEGIN: test0_destroySynchronizedQueue\n");

  // Create the stubbed queue
  SynchronizedQueue* queue = malloc(sizeof(SynchronizedQueue));

  // Properly initialize the queue
  queue->first = NULL;
  queue->last  = NULL;
  mtx_init(&queue->concurrencyMutex, mtx_plain);
  sem_init(&queue->queueFillSemaphore, 0, 0);

  // Destroy stubbed queue
  destroySynchronizedQueue(queue);

  // Log that we passed this testcase
  printf("END: test0_destroySynchronizedQueue\n");
}

// Test if push fails properly on NULL queue
void test0_pushToSynchronizedQueue()
{
  // Log that we started this testcase
  printf("BEGIN: test0_pushToSynchronizedQueue\n");

  // Fail if the call didn't fail as expected
  assert(pushToSynchronizedQueue(NULL, 128, NULL) == PUSH_TO_SYNCHRONIZED_QUEUE_INVALID_VALUE);

  // Log that we passed this testcase
  printf("END: test0_pushToSynchronizedQueue\n");
}

// Test if element can be pushed to SynchronizedQueue
void test1_pushToSynchronizedQueue()
{
  // Log that we started this testcase
  printf("BEGIN: test1_pushToSynchronizedQueue\n");

  // Create the queue
  SynchronizedQueue* queue = createSynchronizedQueue();

  // Fail if queue is NULL
  assert(queue != NULL);

  // Fail if push has failed
  assert(pushToSynchronizedQueue(queue, 128, NULL) == 0);

  // Fail if queue first element is NULL after push
  assert(queue->first != NULL);

  // Fail if queue last element is NULL after push
  assert(queue->last != NULL);

  // Fail if queue last element is not the same as first alement after single push
  assert(queue->last == queue->first);

  // Fail if id or userData are different than pushed ones
  assert(queue->first->id == 128);
  assert(queue->first->userData == NULL);

  // Destroy the queue
  destroySynchronizedQueue(queue);

  // Log that we passed this testcase
  printf("END: test1_pushToSynchronizedQueue\n");
}

// Test if element can be pushed to shutdown SynchronizedQueue
void test2_pushToSynchronizedQueue()
{
  // Log that we started this testcase
  printf("BEGIN: test2_pushToSynchronizedQueue\n");

  // Create the queue
  SynchronizedQueue* queue = createSynchronizedQueue();

  // Fail if queue is NULL
  assert(queue != NULL);

  // Shutdown the queue
  shutdownSynchronizedQueue(queue);

  // Fail if push has failed
  assert(pushToSynchronizedQueue(queue, 128, NULL) == PUSH_TO_SYNCHRONIZED_QUEUE_SHUTDOWN);

  // Fail if queue first element is still NULL after failed push
  assert(queue->first == NULL);

  // Fail if queue last element is still NULL after failed push
  assert(queue->last == NULL);

  // Destroy the queue
  destroySynchronizedQueue(queue);

  // Log that we passed this testcase
  printf("END: test2_pushToSynchronizedQueue\n");
}

// Test if pop fails properly on NULL queue
void test0_popFromSynchronizedQueue()
{
  // Log that we started this testcase
  printf("BEGIN: test0_popFromSynchronizedQueue\n");

  // Initialize data
  unsigned short id = 128;
  void* userData    = NULL;

  // Fail if the call didn't fail as expected
  assert(popFromSynchronizedQueue(NULL, &id, &userData) == POP_FROM_SYNCHRONIZED_QUEUE_INVALID_VALUE);

  // Fail if id changed
  assert(id == 128);

  // Fail if userData pointer changed
  assert(userData == NULL);

  // Log that we passed this testcase
  printf("END: test0_popFromSynchronizedQueue\n");
}

// Test if pop fails properly on shutdown queue
void test1_popFromSynchronizedQueue()
{
  // Log that we started this testcase
  printf("BEGIN: test1_popFromSynchronizedQueue\n");

  // Initialize data
  unsigned short id = 128;
  void* userData    = NULL;

  // Create the queue
  SynchronizedQueue* queue = createSynchronizedQueue();

  // Fail if queue is NULL
  assert(queue != NULL);

  shutdownSynchronizedQueue(queue);

  // Fail if the call didn't fail as expected
  assert(popFromSynchronizedQueue(queue, &id, &userData) == POP_FROM_SYNCHRONIZED_QUEUE_SHUTDOWN);

  // Fail if id changed
  assert(id == 128);

  // Fail if userData pointer changed
  assert(userData == NULL);

  // Log that we passed this testcase
  printf("END: test1_popFromSynchronizedQueue\n");
}

int main()
{
  // Run all tests
  // createSynchronizedQueue()
  test0_createSynchronizedQueue();

  // destroySynchronizedQueue()
  test0_destroySynchronizedQueue();

  // pushToSynchronizedQueue()
  test0_pushToSynchronizedQueue();
  test1_pushToSynchronizedQueue();
  test2_pushToSynchronizedQueue();

  // popFromSynchronizedQueue
  test0_popFromSynchronizedQueue();
  test1_popFromSynchronizedQueue();

  // Return success if all tests passed
  return 0;
}