#include <assert.h> // Defines assert()
#include <stdio.h>  // Defines printf()

// Include Code under Test
#include "procStatDataAnalyzer.c"
#include "processAnalyzer/procStatData.h"
#include "processAnalyzer/procStatDataAnalyzer.h"

// Test if NULL procStat argument results in proper error
void test0_analyzeProcStatData()
{
  // Log that we started this testcase
  printf("BEGIN: test0_analyzeProcStatData\n");

  // Initialize function input
  AnalyzedData* out = NULL;

  // Fail if return code is different that expected
  assert(analyzeProcStatData(4, NULL, NULL, &out) == ANALYZE_PROC_STAT_DATA_INVALID_ARGUMENT);

  // Fail if out pointer has changed from NULL
  assert(out == NULL);

  // Log that we passed this testcase
  printf("END: test0_analyzeProcStatData\n");
}

// Test if NULL data argument results in proper error
void test1_analyzeProcStatData()
{
  // Log that we started this testcase
  printf("BEGIN: test1_analyzeProcStatData\n");

  // Initialize function input for 2 CPUs
  ProcStatData* prev = malloc(sizeof(ProcStatData) + 2 * sizeof(CpuLoad));
  ProcStatData* curr = malloc(sizeof(ProcStatData) + 2 * sizeof(CpuLoad));
  memset(prev, 0, sizeof(ProcStatData) + 2 * sizeof(CpuLoad));
  memset(curr, 0, sizeof(ProcStatData) + 2 * sizeof(CpuLoad));

  // Number of CPUs in structure is 2 for the test
  prev->cpuCount = 2;
  // All CPUs load is 20/5/5/70
  prev->aggregated.user   = 2000;
  prev->aggregated.nice   = 500;
  prev->aggregated.system = 500;
  prev->aggregated.idle   = 7000;
  // CPU #0 load is 40/0/0/60
  prev->cpu[0].user   = 2000;
  prev->cpu[0].nice   = 0;
  prev->cpu[0].system = 0;
  prev->cpu[0].idle   = 3000;
  // CPU #1 load is 0/10/10/80
  prev->cpu[1].user   = 0;
  prev->cpu[1].nice   = 500;
  prev->cpu[1].system = 500;
  prev->cpu[1].idle   = 4000;

  // Number of CPUs in structure is 2 for the test
  curr->cpuCount = 2;
  // All CPUs load is 20/5/5/70
  curr->aggregated.user   = 4000;
  curr->aggregated.nice   = 1000;
  curr->aggregated.system = 1000;
  curr->aggregated.idle   = 14000;
  // CPU #0 load is 40/0/0/60
  curr->cpu[0].user   = 4000;
  curr->cpu[0].nice   = 0;
  curr->cpu[0].system = 0;
  curr->cpu[0].idle   = 6000;
  // CPU #1 load is 0/10/10/80
  curr->cpu[1].user   = 0;
  curr->cpu[1].nice   = 1000;
  curr->cpu[1].system = 1000;
  curr->cpu[1].idle   = 8000;

  // Fail if return code is different that expected
  assert(analyzeProcStatData(4, prev, curr, NULL) == ANALYZE_PROC_STAT_DATA_INVALID_ARGUMENT);

  // Remember to free the memory allocated previously
  free(prev);
  free(curr);

  // Log that we passed this testcase
  printf("END: test1_analyzeProcStatData\n");
}

// Test if procStat argument with cpuCount equal 0 results in proper error
void test2_analyzeProcStatData()
{
  // Log that we started this testcase
  printf("BEGIN: test2_analyzeProcStatData\n");

  // Initialize function input for 0 CPUs
  AnalyzedData* out  = NULL;
  ProcStatData* prev = malloc(sizeof(ProcStatData) + 2 * sizeof(CpuLoad));
  ProcStatData* curr = malloc(sizeof(ProcStatData) + 2 * sizeof(CpuLoad));
  memset(prev, 0, sizeof(ProcStatData) + 2 * sizeof(CpuLoad));
  memset(curr, 0, sizeof(ProcStatData) + 2 * sizeof(CpuLoad));

  // Number of CPUs in structure is 0 for the test
  prev->cpuCount = 0;
  // All CPUs load is 20/5/5/70
  prev->aggregated.user   = 2000;
  prev->aggregated.nice   = 500;
  prev->aggregated.system = 500;
  prev->aggregated.idle   = 7000;
  // CPU #0 load is 40/0/0/60
  prev->cpu[0].user   = 2000;
  prev->cpu[0].nice   = 0;
  prev->cpu[0].system = 0;
  prev->cpu[0].idle   = 3000;
  // CPU #1 load is 0/10/10/80
  prev->cpu[1].user   = 0;
  prev->cpu[1].nice   = 500;
  prev->cpu[1].system = 500;
  prev->cpu[1].idle   = 4000;

  // Number of CPUs in structure is 2 for the test
  curr->cpuCount = 2;
  // All CPUs load is 20/5/5/70
  curr->aggregated.user   = 4000;
  curr->aggregated.nice   = 1000;
  curr->aggregated.system = 1000;
  curr->aggregated.idle   = 14000;
  // CPU #0 load is 40/0/0/60
  curr->cpu[0].user   = 4000;
  curr->cpu[0].nice   = 0;
  curr->cpu[0].system = 0;
  curr->cpu[0].idle   = 6000;
  // CPU #1 load is 0/10/10/80
  curr->cpu[1].user   = 0;
  curr->cpu[1].nice   = 1000;
  curr->cpu[1].system = 1000;
  curr->cpu[1].idle   = 8000;

  // Fail if return code is different that expected
  assert(analyzeProcStatData(4, prev, curr, &out) == ANALYZE_PROC_STAT_DATA_INVALID_ARGUMENT);

  // Remember to free the memory allocated previously
  free(prev);
  free(curr);

  // Log that we passed this testcase
  printf("END: test2_analyzeProcStatData\n");
}

// Test if example data is parsed properly
void test3_analyzeProcStatData()
{
  // Log that we started this testcase
  printf("BEGIN: test3_analyzeProcStatData\n");
  // Initialize function input for 2 CPUs
  AnalyzedData* out  = NULL;
  ProcStatData* prev = malloc(sizeof(ProcStatData) + 2 * sizeof(CpuLoad));
  ProcStatData* curr = malloc(sizeof(ProcStatData) + 2 * sizeof(CpuLoad));
  memset(prev, 0, sizeof(ProcStatData) + 2 * sizeof(CpuLoad));
  memset(curr, 0, sizeof(ProcStatData) + 2 * sizeof(CpuLoad));

  // Number of CPUs in structure is 0 for the test
  prev->cpuCount = 2;
  // All CPUs load is 20/5/5/70
  prev->aggregated.user   = 2000;
  prev->aggregated.nice   = 500;
  prev->aggregated.system = 500;
  prev->aggregated.idle   = 7000;
  // CPU #0 load is 40/0/0/60
  prev->cpu[0].user   = 2000;
  prev->cpu[0].nice   = 0;
  prev->cpu[0].system = 0;
  prev->cpu[0].idle   = 3000;
  // CPU #1 load is 0/10/10/80
  prev->cpu[1].user   = 0;
  prev->cpu[1].nice   = 500;
  prev->cpu[1].system = 500;
  prev->cpu[1].idle   = 4000;

  // Number of CPUs in structure is 2 for the test
  curr->cpuCount = 2;
  // All CPUs load is 20/5/5/70
  curr->aggregated.user   = 4000;
  curr->aggregated.nice   = 1000;
  curr->aggregated.system = 1000;
  curr->aggregated.idle   = 14000;
  // CPU #0 load is 40/0/0/60
  curr->cpu[0].user   = 4000;
  curr->cpu[0].nice   = 0;
  curr->cpu[0].system = 0;
  curr->cpu[0].idle   = 6000;
  // CPU #1 load is 0/10/10/80
  curr->cpu[1].user   = 0;
  curr->cpu[1].nice   = 1000;
  curr->cpu[1].system = 1000;
  curr->cpu[1].idle   = 8000;

  // Fail if return code is different that expected
  assert(analyzeProcStatData(4, prev, curr, &out) == 0);

  // Remember to free the memory allocated previously
  free(prev);
  free(curr);

  // Fail if out pointer has not changed from NULL
  assert(out != NULL);

  // Fail if precision is other than expected
  assert(out->precision == 4);

  // Fail if analyzed data is different than expected (according to precision)
  assert(out->average.user == 200000);  // 20.0000%
  assert(out->average.nice == 50000);   // 05.0000%
  assert(out->average.system == 50000); // 05.0000%
  assert(out->average.idle == 700000);  // 70.0000%
  assert(out->cpu[0].user == 400000);   // 40.0000%
  assert(out->cpu[0].nice == 0);        // 00.0000%
  assert(out->cpu[0].system == 0);      // 00.0000%
  assert(out->cpu[0].idle == 600000);   // 60.0000%
  assert(out->cpu[1].user == 0);        // 00.0000%
  assert(out->cpu[1].nice == 100000);   // 10.0000%
  assert(out->cpu[1].system == 100000); // 10.0000%
  assert(out->cpu[1].idle == 800000);   // 80.0000%

  // Make sure to release memory
  free(out);

  // Log that we passed this testcase
  printf("END: test3_analyzeProcStatData\n");
}

int main()
{
  // Run all tests
  test0_analyzeProcStatData();
  test1_analyzeProcStatData();
  test2_analyzeProcStatData();
  test3_analyzeProcStatData();

  // Return success if all tests passed
  return 0;
}