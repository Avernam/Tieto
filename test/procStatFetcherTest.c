#include <assert.h> // Defines assert()
#include <stdio.h>  // Defines printf()

// Include Code under Test
#include "procStatFetcher.c"

// Test if CPU Count equal 0 will return expected error
void test0_readProcStat()
{
  // Log that we started this testcase
  printf("BEGIN: test0_readProcStat\n");

  // Prepare a variable to point to extracted data
  char* buffer = NULL;

  // Fail if readProcStat is not failing as expected
  assert(readProcStat(0, &buffer) == FETCH_CPU_LOAD_INVALID_VALUE);

  // Fail if buffer is no longer NULL
  assert(buffer == NULL);

  printf("END: test0_readProcStat\n");
}

// Test if buffer pointer equal NULL will return expected error
void test1_readProcStat()
{
  // Log that we started this testcase
  printf("BEGIN: test1_readProcStat\n");

  // Fail if readProcStat is not failing as expected
  assert(readProcStat(1, NULL) == FETCH_CPU_LOAD_INVALID_VALUE);

  printf("END: test1_readProcStat\n");
}

// Test if proper buffer is returned
void test2_readProcStat()
{
  // Log that we started this testcase
  printf("BEGIN: test2_readProcStat\n");

  // Prepare a variable to point to extracted data
  char* buffer = NULL;

  // Assume single core machine
  // Fail if readProcStat is not success
  assert(readProcStat(1, &buffer) == 0);

  // Fail if buffer is still NULL
  assert(buffer != NULL);

  // Hold the current line of buffer starting at first
  char* currentLine = buffer;

  // Placeholder variables to be dumped
  unsigned int user, nice, system, idle;

  // Fail if aggregated data is not in expected format (i.e. not all fields are read)
  assert(sscanf(currentLine, "cpu %u %u %u %u", &user, &nice, &system, &idle) == 4);

  // Move to second line
  currentLine = strchr(currentLine, '\n');

  // Fail if there is no second line
  assert(currentLine != NULL);

  // Move past newline character
  currentLine++;

  // Fail if per-CPU data is not in expected format (i.e. not all fields are read)
  assert(sscanf(currentLine, "cpu%*u %u %u %u %u", &user, &nice, &system, &idle) == 4);

  // Free owned buffer
  free(buffer);
  printf("END: test2_readProcStat\n");
}

// Test if CPU Count equal 0 will return expected error
void test0_parseProcStatData()
{
  // Log that we started this testcase
  printf("BEGIN: test0_parseProcStatData\n");

  // Prepare a variable to point to extracted data
  ProcStatData* cpuLoad = NULL;

  // Fail if readProcStat is not failing as expected
  assert(parseProcStatData(0, "cpu 1 1 1 1\ncpu0 1 1 1 1", &cpuLoad) == FETCH_CPU_LOAD_INVALID_VALUE);

  // Fail if buffer is no longer NULL
  assert(cpuLoad == NULL);

  // Log that we passed this testcase
  printf("END: test0_parseProcStatData\n");
}

// Test if NULL input will return expected error
void test1_parseProcStatData()
{
  // Log that we started this testcase
  printf("BEGIN: test1_parseProcStatData\n");

  // Prepare a variable to point to extracted data
  ProcStatData* cpuLoad = NULL;

  // Fail if readProcStat is not failing as expected
  assert(parseProcStatData(1, NULL, &cpuLoad) == FETCH_CPU_LOAD_INVALID_VALUE);

  // Fail if buffer is no longer NULL
  assert(cpuLoad == NULL);

  // Log that we passed this testcase
  printf("END: test1_parseProcStatData\n");
}

// Test if NULL output will return expected error
void test2_parseProcStatData()
{
  // Log that we started this testcase
  printf("BEGIN: test2_parseProcStatData\n");

  // Fail if readProcStat is not failing as expected
  assert(parseProcStatData(1, "cpu 1 1 1 1\ncpu0 1 1 1 1", NULL) == FETCH_CPU_LOAD_INVALID_VALUE);

  // Log that we passed this testcase
  printf("END: test2_parseProcStatData\n");
}

// Test if parsing is done correctly for partial data
void test3_parseProcStatData()
{
  // Log that we started this testcase
  printf("BEGIN: test3_parseProcStatData\n");

  // Prepare a variable to point to extracted data
  ProcStatData* cpuLoad = NULL;

  // Fail if readProcStat is not failing as expected
  assert(parseProcStatData(1, "cpu 2 3 4 5\ncpu0 6 7 8 9\n", &cpuLoad) == 0);

  // Fail if buffer is NULL
  assert(cpuLoad != NULL);

  // Check if we have properly parsed the data
  assert(cpuLoad->cpuCount == 1);
  assert(cpuLoad->aggregated.user == 2);
  assert(cpuLoad->aggregated.nice == 3);
  assert(cpuLoad->aggregated.system == 4);
  assert(cpuLoad->aggregated.idle == 5);
  assert(cpuLoad->aggregated.ioWait == 0);
  assert(cpuLoad->aggregated.irq == 0);
  assert(cpuLoad->aggregated.softIrq == 0);
  assert(cpuLoad->aggregated.steal == 0);
  assert(cpuLoad->aggregated.guest == 0);
  assert(cpuLoad->aggregated.guestNice == 0);
  assert(cpuLoad->cpu[0].user == 6);
  assert(cpuLoad->cpu[0].nice == 7);
  assert(cpuLoad->cpu[0].system == 8);
  assert(cpuLoad->cpu[0].idle == 9);
  assert(cpuLoad->cpu[0].ioWait == 0);
  assert(cpuLoad->cpu[0].irq == 0);
  assert(cpuLoad->cpu[0].softIrq == 0);
  assert(cpuLoad->cpu[0].steal == 0);
  assert(cpuLoad->cpu[0].guest == 0);
  assert(cpuLoad->cpu[0].guestNice == 0);

  // Free allocated memory
  free(cpuLoad);

  // Log that we passed this testcase
  printf("END: test3_parseProcStatData\n");
}

// Test if parsing is done correctly for full data
void test4_parseProcStatData()
{
  // Log that we started this testcase
  printf("BEGIN: test4_parseProcStatData\n");

  // Prepare a variable to point to extracted data
  ProcStatData* cpuLoad = NULL;

  // Fail if readProcStat is not failing as expected
  assert(parseProcStatData(1, "cpu 0 1 2 3 4 5 6 7 8 9\ncpu0 9 8 7 6 5 4 3 2 1\n", &cpuLoad) == 0);

  // Fail if buffer is NULL
  assert(cpuLoad != NULL);

  // Check if we have properly parsed the data
  assert(cpuLoad->cpuCount == 1);
  assert(cpuLoad->aggregated.user == 0);
  assert(cpuLoad->aggregated.nice == 1);
  assert(cpuLoad->aggregated.system == 2);
  assert(cpuLoad->aggregated.idle == 3);
  assert(cpuLoad->aggregated.ioWait == 4);
  assert(cpuLoad->aggregated.irq == 5);
  assert(cpuLoad->aggregated.softIrq == 6);
  assert(cpuLoad->aggregated.steal == 7);
  assert(cpuLoad->aggregated.guest == 8);
  assert(cpuLoad->aggregated.guestNice == 9);
  assert(cpuLoad->cpu[0].user == 9);
  assert(cpuLoad->cpu[0].nice == 8);
  assert(cpuLoad->cpu[0].system == 7);
  assert(cpuLoad->cpu[0].idle == 6);
  assert(cpuLoad->cpu[0].ioWait == 5);
  assert(cpuLoad->cpu[0].irq == 4);
  assert(cpuLoad->cpu[0].softIrq == 3);
  assert(cpuLoad->cpu[0].steal == 2);
  assert(cpuLoad->cpu[0].guest == 1);
  assert(cpuLoad->cpu[0].guestNice == 0);

  // Free allocated memory
  free(cpuLoad);

  // Log that we passed this testcase
  printf("END: test4_parseProcStatData\n");
}

void test0_fetchCpuLoad()
{
  // Log that we started this testcase
  printf("BEGIN: test0_fetchCpuLoad\n");

  // Prepare a variable to point to extracted data
  ProcStatData* cpuLoad = NULL;

  // Fail if return is not success
  assert(fetchCpuLoad(&cpuLoad) == 0);

  // Fail if CPU Load buffer is NULL
  assert(cpuLoad != NULL);

  // Fail if number of CPUs is less or equal 0
  assert(cpuLoad->cpuCount > 0);

  // Free allocated memory
  free(cpuLoad);

  // Log that we passed this testcase
  printf("END: test0_fetchCpuLoad\n");
}

int main()
{
  // Run all tests
  // readProcStat()
  test0_readProcStat();
  test1_readProcStat();
  test2_readProcStat();

  // parseProcStatData()
  test0_parseProcStatData();
  test1_parseProcStatData();
  test2_parseProcStatData();
  test3_parseProcStatData();
  test4_parseProcStatData();

  // fetchCpuLoad()
  test0_fetchCpuLoad();

  // Return success if all tests passed
  return 0;
}