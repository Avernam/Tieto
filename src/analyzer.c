// Implemented API
#include "processAnalyzer/analyzer.h"

// Local API
#include "processAnalyzer/procStatDataAnalyzer.h" // Defines analyzeProcStatData()
#include "processAnalyzer/synchronizedQueue.h"    // Defines SynchronizedQueue operations

// System API
#include <stdlib.h>  // Defines free()
#include <threads.h> // Defines thrd_exit()

// Forward Declaration
struct ProcStatData;

int analyzer(void* context)
{
  // Extract our context
  AnalyzerContext* ctx = (AnalyzerContext*)context;

  // Prepare place to receive data from Reader
  unsigned short msgId             = 0;
  struct ProcStatData* procStat[2] = { NULL, NULL }; // This is a ping-pong buffer

  // Initialize pingPong buffer indices and states
  int pingPongSide           = 0;
  int receivedFirstSetOfData = 0;

  // While we can get information from reader analyze the data
  while (popFromSynchronizedQueue(ctx->inQueue, &msgId, (void**)&procStat[pingPongSide]) == 0)
  {
    // Mark when we receive first full set of data for the first time
    if (receivedFirstSetOfData == 0 && pingPongSide == 1)
    {
      receivedFirstSetOfData = 1;
    }

    // Determine which buffer is previous and which one is current puls swap side on pingPong
    int currentPingPongSide = pingPongSide;              // This indicates "current set of data"
    pingPongSide            = pingPongSide == 0 ? 1 : 0; // This indicates "previous set of data" to be overwritten at next step

    if (receivedFirstSetOfData == 1)
    {
      // Prepare place to grab output of analysis
      struct AnalyzedData* analyzedData = NULL;

      // Analyze 2 subsequent sets of data
      if (analyzeProcStatData(ctx->precision, procStat[pingPongSide], procStat[currentPingPongSide], &analyzedData) != 0)
      {
        // Free all the buffers as we are abandoning the thread
        free(procStat[0]);
        free(procStat[1]);

        // Abandon thread with error
        thrd_exit(-1);
        return -1; // For compiler
      }

      // At this point we analyzed the data successfully
      int pushError = pushToSynchronizedQueue(ctx->outQueue, 0, analyzedData);

      // If it's shutdown free everything and exit without error
      if (pushError != 0)
      {
        free(procStat[0]);
        free(procStat[1]);
        free(analyzedData);

        // Abandon thread without or with error depending on push error code (shutdown is OK)
        thrd_exit(pushError == PUSH_TO_SYNCHRONIZED_QUEUE_SHUTDOWN ? 0 : -1);
        return pushError == PUSH_TO_SYNCHRONIZED_QUEUE_SHUTDOWN ? 0 : -1; // For compiler
      }
    }

    // Remember to free old unused buffer after we are done
    if (procStat[pingPongSide] != NULL)
    {
      free(procStat[pingPongSide]);
      procStat[pingPongSide] = NULL;
    }
  }

  // Remember to free memory at the end of the day
  if (procStat[0] != NULL)
  {
    free(procStat[0]);
  }

  if (procStat[1] != NULL)
  {
    free(procStat[1]);
  }

  // Return success
  thrd_exit(0);
  return 0; // For compiler
}
