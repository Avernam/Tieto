// Implemented API
#include "processAnalyzer/procStatFetcher.h"

// Local API
#include "processAnalyzer/procStatData.h"

// System API
#include <fcntl.h>       // Defines open()
#include <stdio.h>       // Defines sscanf()
#include <stdlib.h>      // Defines malloc()
#include <string.h>      // Defines strchr()
#include <sys/sysinfo.h> // Defines get_nprocs()
#include <sys/types.h>   // Defines read()
#include <unistd.h>      // Defines NULL

#define MAX_SIZE_OF_CPU_STR        6  // cpuxxx, where xxx is assumed to be 0-255
#define MAX_SIZE_OF_INT_STR        20 // Number of characters in "18446744073709551615"
#define NUM_OF_COLUMNS_PER_CPU     10 // Number of columns per single /proc/stat CPU entry
#define NUM_OF_WHITESPACES_PER_CPU 11 // number of all whitespaces in CPU string (including '\n')

// The maximum size of a single CPU load entry in "/proc/stat" according to documentation (till date 22.06.2022)
#define MAX_ENTRY_SIZE (MAX_SIZE_OF_CPU_STR + (MAX_SIZE_OF_INT_STR * NUM_OF_COLUMNS_PER_CPU) + NUM_OF_WHITESPACES_PER_CPU)

// This function reads "/proc/stat" file and returns buffer with contents regarding CPU load in "output"
// Caller is responsible for clearing the memory if the call suceeds.
// Return 0 on success.
// Return < 0 (proper FETCH_CPU_LOAD_ error code) on fail
static int readProcStat(int cpuCount, char** output)
{
  // If we don't have pointer for output or there are no CPUs return error
  if (output == NULL || cpuCount <= 0)
  {
    return FETCH_CPU_LOAD_INVALID_VALUE;
  }

  // Open /proc/stat in Read-Only mode
  int fd = open("/proc/stat", O_RDONLY);

  if (fd < 0)
  {
    return FETCH_CPU_LOAD_FAILED_TO_OPEN_SOURCE;
  }

  // Calculate size of the string we do need to read
  // +1 to cpuCount account for 'aggregated' entry (which is first entry)
  size_t readSize = (cpuCount + 1) * MAX_ENTRY_SIZE;

  // Allocate proper buffer. +1 to add trailing '\0' for parsing safety
  char* buffer = malloc(readSize + 1);

  // Safely initialize memory
  memset(buffer, 0, readSize + 1);

  // Read the data from "fd"
  if (read(fd, (void*)buffer, readSize) < 0)
  {
    // Remember to free memory!
    free(buffer);

    // And close File Descriptor
    close(fd);
    return FETCH_CPU_LOAD_FAILED_TO_READ_SOURCE;
  }

  // Close File Descriptor as we no longer use it
  close(fd);

  // Hand over the buffer
  *output = buffer;

  // Return success
  return 0;
}

// This parses the buffer in procfs stat format into ProcStatData.
// After the call on success caller is responsible for the memory pointed by *cpuLoad.
// Return 0 on success.
// Return < 0 (proper FETCH_CPU_LOAD_ error code) on fail
static int parseProcStatData(int cpuCount, const char* buffer, ProcStatData** cpuLoad)
{
  // If we don't have pointer for output or there are no CPUs return error or there is no input buffer
  if (cpuLoad == NULL || cpuCount <= 0 || buffer == NULL)
  {
    return FETCH_CPU_LOAD_INVALID_VALUE;
  }

  // Prepare buffer for /proc/stat data based on number of online CPUs
  ProcStatData* procStat = malloc(sizeof(ProcStatData) + cpuCount * sizeof(CpuLoad));
  // Initialize it with 0's
  memset(procStat, 0, sizeof(ProcStatData) + cpuCount * sizeof(CpuLoad));

  // Save previously read CPU count
  procStat->cpuCount = cpuCount;

  // First get the 'aggregated' load data -> cast to void as we are not interested in return value
  (void)sscanf(buffer,
               "cpu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu",
               &procStat->aggregated.user,
               &procStat->aggregated.nice,
               &procStat->aggregated.system,
               &procStat->aggregated.idle,
               &procStat->aggregated.ioWait,
               &procStat->aggregated.irq,
               &procStat->aggregated.softIrq,
               &procStat->aggregated.steal,
               &procStat->aggregated.guest,
               &procStat->aggregated.guestNice);

  // Move buffer to the next line
  // For the purpose of moving pointer we need separate one
  // strchr searches for the next character and returns pointer 'c' to it (in this case 'c' is '\n')
  char* currentLine = strchr(buffer, '\n');

  // If there is no next line something went wrong
  if (currentLine == NULL)
  {
    // Remember to free allocated and not returned data
    free(procStat);
    return FETCH_CPU_LOAD_MALFORMED_SOURCE_DATA;
  }

  // Now Loop over read CPU count
  for (int cpuNo = 0; cpuNo < cpuCount; cpuNo++)
  {
    // Move pointer past '\n' character
    currentLine += 1;

    // Now get data for each CPU separetely
    // Note %*u modified - we ignore CPU number this way
    (void)sscanf(currentLine,
                 "cpu%*u %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu",
                 &procStat->cpu[cpuNo].user,
                 &procStat->cpu[cpuNo].nice,
                 &procStat->cpu[cpuNo].system,
                 &procStat->cpu[cpuNo].idle,
                 &procStat->cpu[cpuNo].ioWait,
                 &procStat->cpu[cpuNo].irq,
                 &procStat->cpu[cpuNo].softIrq,
                 &procStat->cpu[cpuNo].steal,
                 &procStat->cpu[cpuNo].guest,
                 &procStat->cpu[cpuNo].guestNice);

    // Move buffer to the next line
    currentLine = strchr(currentLine, '\n');

    // If there is no next line something went wrong
    // This works also for last CPU as we expect further data in /proc/stat
    if (currentLine == NULL)
    {
      // Remember to free allocated and not returned data
      free(procStat);
      return FETCH_CPU_LOAD_MALFORMED_SOURCE_DATA;
    }
  }

  // Return our read data
  *cpuLoad = procStat;

  // Note that there were no errors
  return 0;
}

int fetchCpuLoad(ProcStatData** cpuLoad)
{
  // Initialize error to success
  int error = 0;

  // Pointer to "proc/stat" data buffer we are going to get
  char* buffer = NULL;

  // Get number of online CPUs
  int cpuCount = get_nprocs();

  // If error will be equal 0, then buffer will be our data
  error = readProcStat(cpuCount, &buffer);

  // If reading "/proc/stat" fails hand over the error code
  if (error != 0)
  {
    return error;
  }

  // Parse the data from "/proc/stat" into the buffer supplemented by the caller
  error = parseProcStatData(cpuCount, buffer, cpuLoad);

  // Free the buffer containing raw data from "/proc/stat"
  free(buffer);

  // Return the error as determined by parser
  return error;
}
