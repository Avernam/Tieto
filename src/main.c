// Local API
#include "processAnalyzer/analyzer.h"
#include "processAnalyzer/printer.h"
#include "processAnalyzer/reader.h"
#include "processAnalyzer/synchronizedQueue.h" // Defines SynchronizedQueue operations

// System API
#include <signal.h>  // Defines signal(), SIGINT, SIGTERM
#include <stdio.h>   // Defines printf()
#include <threads.h> // Defines thrd_t, thrd_create(), thrd_join()

// Configuration how often data should be read by Reader (and by effect printed by Printer)
// Note that low values have really bad resolution, even though whole range of values does work (including 0)
#define CONFIG_REFRESH_RATE_MS 1000U
// Configuration how precise Analyser should be (0-5)
#define CONFIG_ANALYZER_PRECISION 4U
// Configuration how precise Printer should be (0-5) - currently has no effect (it's always 2)
#define CONFIG_PRINTER_PRECISION 2U
// Config how many SynchronizedQueue(s) do we serve - this value is only for signal handler to end program gracefully
#define CONFIG_NUMBER_OF_QUEUES 2U

static struct SynchronizedQueue* queues[CONFIG_NUMBER_OF_QUEUES] = { NULL, NULL };

static void signalCatcher(int signalNumber)
{
  // Ignore signal number
  (void)signalNumber;

  // In this case shutdown queues. Worst case scenario is that they are already shutdown which has no effect
  // The same goes if they are NULL
  shutdownSynchronizedQueue(queues[0]);
  shutdownSynchronizedQueue(queues[1]);
}

int main()
{
  // Create SynchronizedQueues
  struct SynchronizedQueue* readerAnalyzerQ  = createSynchronizedQueue();
  struct SynchronizedQueue* analyzerPrinterQ = createSynchronizedQueue();

  // Save Queues for signalCatcher()
  queues[0] = readerAnalyzerQ;
  queues[1] = analyzerPrinterQ;

  // Register signal handler for Interrupt and Terminate
  signal(SIGINT, signalCatcher);
  signal(SIGTERM, signalCatcher);

  // Create thread contexts
  ReaderContext readerCtx     = { .outQueue = readerAnalyzerQ, .readIntervalMs = CONFIG_REFRESH_RATE_MS };
  AnalyzerContext analyzerCtx = { .inQueue = readerAnalyzerQ, .outQueue = analyzerPrinterQ, .precision = CONFIG_ANALYZER_PRECISION };
  PrinterContext printerCtx   = { .inQueue = analyzerPrinterQ, .precision = CONFIG_PRINTER_PRECISION };

  // Create threads
  thrd_t readerThr, analyzerThr, printerThr;
  (void)thrd_create(&readerThr, reader, &readerCtx);
  (void)thrd_create(&analyzerThr, analyzer, &analyzerCtx);
  (void)thrd_create(&printerThr, printer, &printerCtx);

  // Join order is important - in case of any issues Reader ends first and kills all other threads in effect
  int retVal = 0;
  thrd_join(readerThr, &retVal);

  // In this case shutdown queues. Worst case scenario is that they are already shutdown which has no effect
  shutdownSynchronizedQueue(readerAnalyzerQ);
  shutdownSynchronizedQueue(analyzerPrinterQ);

  // Now wait for other threads
  thrd_join(analyzerThr, &retVal);
  thrd_join(printerThr, &retVal);

  // Reset Queues for signalCatcher()
  queues[0] = NULL;
  queues[1] = NULL;

  // Free the queues
  destroySynchronizedQueue(readerAnalyzerQ);
  destroySynchronizedQueue(analyzerPrinterQ);

  // And we are done!
  return 0;
}
