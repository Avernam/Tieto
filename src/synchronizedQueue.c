// Implemented API
#include "processAnalyzer/synchronizedQueue.h"

// System API
#include <semaphore.h> // Defines sem_t, sem_init(), sem_wait(), sem_post(), sem_destroy()
#include <stdlib.h>    // Defines malloc()
#include <threads.h>   // Defines mtx_t, mtx_init(), mtx_lock(), mtx_unlock(), mtx_destroy()

// This structure holds a single element in queue
typedef struct SynchronizedQueueElement
{
  // ID which will be used by consumer and is set by producer
  unsigned short id;
  // Data passed by producer to consumer
  void* userData;
  // Pointer to next element in queue
  struct SynchronizedQueueElement* next;
} SynchronizedQueueElement;

typedef struct SynchronizedQueue
{
  // Indicates if queue is shut down
  int shutdown;
  // Protects the queue from simultanious access
  mtx_t concurrencyMutex;
  // Used to block pop on empty queue until queue gets filled
  sem_t queueFillSemaphore;
  // Point of consumption (we pop from here)
  SynchronizedQueueElement* first;
  // Point of production (we push to here)
  SynchronizedQueueElement* last;
} SynchronizedQueue;

SynchronizedQueue* createSynchronizedQueue()
{
  // Allocate space for the queue
  SynchronizedQueue* queue = malloc(sizeof(SynchronizedQueue));

  // Initialize points to NULL
  queue->first = NULL;
  queue->last  = NULL;

  // Indicate that queue is currently working
  queue->shutdown = 0;

  // Initialize concurrency guards - we assume these can't fail
  (void)mtx_init(&queue->concurrencyMutex, mtx_plain);
  (void)sem_init(&queue->queueFillSemaphore, 0, 0);

  // Return initialized SynchronizedQueue
  return queue;
}

void shutdownSynchronizedQueue(SynchronizedQueue* queue)
{
  // If queue is NULL do nothing
  if (queue == NULL)
  {
    return;
  }

  // Assume that lock cannot fail and lock out any producers/consumers
  // Enter Critical Section
  (void)mtx_lock(&queue->concurrencyMutex);

  // Store shutdown status
  int wasAlreadyShutdown = queue->shutdown;

  // Indicate that queue went into shutdown
  queue->shutdown = 1;

  // Exit Critical Section
  mtx_unlock(&queue->concurrencyMutex);

  // If we went into shutdown for the first time let consumers out
  if (wasAlreadyShutdown == 0)
  {
    // Increment semaphore to unlock consumers
    sem_post(&queue->queueFillSemaphore);
  }
}

void destroySynchronizedQueue(SynchronizedQueue* queue)
{
  // If queue is NULL do nothing
  if (queue == NULL)
  {
    return;
  }

  // At this point we assume that queue is already unused
  while (queue->first != NULL)
  {
    // Save the element we are clearing
    SynchronizedQueueElement* toClear = queue->first;

    // Prevent leaks - free the user data on destroy
    if (toClear->userData != NULL)
    {
      free(toClear->userData);
    }

    // Move the "first" pointer to next element
    queue->first = toClear->next;

    // Actually drop the element
    free(toClear);
  }

  // Destroy the concurrency guards
  mtx_destroy(&queue->concurrencyMutex);
  sem_destroy(&queue->queueFillSemaphore);

  // In the end delete queue itself
  free(queue);
}

int pushToSynchronizedQueue(SynchronizedQueue* queue, unsigned short id, void* userData)
{
  // If queue is NULL return proper error
  if (queue == NULL)
  {
    return PUSH_TO_SYNCHRONIZED_QUEUE_INVALID_VALUE;
  }

  // Create element to be pushed into queue
  SynchronizedQueueElement* element = malloc(sizeof(SynchronizedQueueElement));

  // Fill in the element with the data we received
  element->id       = id;
  element->userData = userData;
  element->next     = NULL;

  // Enter Critical Area
  if (mtx_lock(&queue->concurrencyMutex) != thrd_success)
  {
    // Remember to release the memory
    free(element);
    return PUSH_TO_SYNCHRONIZED_QUEUE_SYNC_ERROR;
  }

  int error = 0;
  // Check queue status (shutdown/empty/non-empty)
  if (queue->shutdown != 0)
  {
    // If queue is in shutdown exit Critical Area and return proper error
    (void)mtx_unlock(&queue->concurrencyMutex);
    // Remember to release the memory
    free(element);
    error = PUSH_TO_SYNCHRONIZED_QUEUE_SHUTDOWN;
  }
  else if (queue->first == NULL)
  {
    // If it is set first and last element as us
    queue->first = element;
    queue->last  = element;
  }
  else
  {
    // If it is not set us as last element (and next after previous last element)
    queue->last->next = element;
    queue->last       = element;
  }

  // Exit Critical Area
  (void)mtx_unlock(&queue->concurrencyMutex);

  if (error == 0)
  {
    // At the end add 1 to semaphore, so consumers might wake up if they are waiting
    sem_post(&queue->queueFillSemaphore);
  }

  return error;
}

int popFromSynchronizedQueue(SynchronizedQueue* queue, unsigned short* id, void** userData)
{
  // If queue is NULL or consumer pointer is NULL return proper error
  if (queue == NULL || id == NULL || userData == NULL)
  {
    return POP_FROM_SYNCHRONIZED_QUEUE_INVALID_VALUE;
  }

  SynchronizedQueueElement* element = NULL;

  // Wait until there is data in queue
  sem_wait(&queue->queueFillSemaphore);

  // Enter Critical Area
  if (mtx_lock(&queue->concurrencyMutex) != thrd_success)
  {
    return POP_FROM_SYNCHRONIZED_QUEUE_SYNC_ERROR;
  }

  // Check queue status (shutdown/empty/non-empty)
  if (queue->shutdown != 0)
  {
    // If queue is in shutdown exit Critical Area and return proper error
    (void)mtx_unlock(&queue->concurrencyMutex);

    // Let next consumer out
    sem_post(&queue->queueFillSemaphore);

    return POP_FROM_SYNCHRONIZED_QUEUE_SHUTDOWN;
  }
  else if (queue->first == NULL)
  {
    // If it is then return proper error
    // But first exit Critical Area
    (void)mtx_unlock(&queue->concurrencyMutex);
    return POP_FROM_SYNCHRONIZED_QUEUE_EMPTY_AFTER_SYNC;
  }
  else
  {
    // If it's not get the first element and move queue
    element      = queue->first;
    queue->first = element->next;
  }

  // Exit Critical Area
  (void)mtx_unlock(&queue->concurrencyMutex);

  // Now extract the data
  *id       = element->id;
  *userData = element->userData;

  // Free the memory allocated for the element
  free(element);

  return 0;
}
