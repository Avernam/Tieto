// Implemented API
#include "processAnalyzer/reader.h"

// Local API
#include "processAnalyzer/procStatFetcher.h"   // Defines fetchCpuLoad()
#include "processAnalyzer/synchronizedQueue.h" // Defines SynchronizedQueue operations

// System API
#include <stdlib.h>  // Defines free()
#include <threads.h> // Defines thrd_sleep(), thrd_exit()
#include <time.h>    // Defines timespec, timespec_get()

// Value of timespec fileds causing wrap around
#define TIMESPEC_SIZE 1000000000L
#define EXTRA_TIME_NS 300 // How much time do we think counting sleep length takes

// Forward declaration
struct ProcStatData;

static void caclulateNextSleepLength(struct timespec* sleepStep,
                                     struct timespec* timeLoopStart,
                                     struct timespec* timeLoopEnd,
                                     struct timespec* sleepFor)
{
  // Determine how long 1st sleep should take, so that we wake up exactly at next 'refresh'
  // Take wrap around into account
  struct timespec timeLoopDiff;
  timeLoopDiff.tv_sec  = (TIMESPEC_SIZE + timeLoopEnd->tv_sec - timeLoopStart->tv_sec) % TIMESPEC_SIZE;
  timeLoopDiff.tv_nsec = (TIMESPEC_SIZE + timeLoopEnd->tv_nsec - timeLoopStart->tv_nsec) % TIMESPEC_SIZE;

  if (sleepStep->tv_sec > 0)
  {
    // If we expect to wait at least a second take a normalized value of seconds
    // After this operation sleepFor.tv_sec is always at least 1
    sleepFor->tv_sec = sleepStep->tv_sec - (timeLoopDiff.tv_sec % sleepStep->tv_sec);

    if (timeLoopDiff.tv_nsec > 0)
    {
      sleepFor->tv_sec -= 1;
      sleepFor->tv_nsec = TIMESPEC_SIZE - timeLoopDiff.tv_nsec - EXTRA_TIME_NS + sleepStep->tv_nsec;

      // This is for cases when we want to wait for e.g. 1.1 ms
      if (sleepFor->tv_nsec > TIMESPEC_SIZE)
      {
        sleepFor->tv_nsec -= TIMESPEC_SIZE;
        sleepFor->tv_sec += 1;
      }
    }
  }
  else if (sleepStep->tv_nsec > 0)
  {
    // If we expect to wait less than a second, then let's calculate nearest window
    // NOTE: For values close to time which is taken by 'fetch' we might miss a single window
    sleepFor->tv_sec  = 0;
    sleepFor->tv_nsec = sleepStep->tv_nsec - (TIMESPEC_SIZE - timeLoopDiff.tv_nsec - EXTRA_TIME_NS) % sleepStep->tv_nsec;
  }
  else
  {
    sleepFor->tv_sec  = 0;
    sleepFor->tv_nsec = 0;
  }
}

int reader(void* context)
{
  ReaderContext* ctx = (ReaderContext*)context;

  // Take current point in time as starting point for sleep operations
  struct timespec timeLoopStart;
  (void)timespec_get(&timeLoopStart, TIME_UTC);

  // Determine how long we will sleep in accepted format
  struct timespec sleepStep;
  sleepStep.tv_sec  = ctx->readIntervalMs / 1000;
  sleepStep.tv_nsec = (ctx->readIntervalMs % 1000) * 1000000UL;

  // Prepare a pointer which will be passed over to queue after each fetch
  struct ProcStatData* procStat = NULL;

  // Make a first read + queue push. In case of fail exit thread
  if (fetchCpuLoad(&procStat) != 0 || pushToSynchronizedQueue(ctx->outQueue, 0, procStat) != 0)
  {
    // If memory for procStat is allocated free it
    if (procStat != NULL)
    {
      free(procStat);
    }

    // In case of fail exit thread immidiately
    thrd_exit(-1);
    return -1; // For compiler
  }

  struct timespec timeLoopEnd;
  (void)timespec_get(&timeLoopEnd, TIME_UTC);

  struct timespec sleepFor;
  caclulateNextSleepLength(&sleepStep, &timeLoopStart, &timeLoopEnd, &sleepFor);

  // This is dummy data holder for the time we didn't slept through, when we received a signal
  struct timespec sleepRemaining;

  // Until we sleeped without interuption continue reading from proc stat
  // This will be interrupted by any signal we receive and return -1
  // If we don't want to wait we assume 'feed as fast as you can'
  while ((sleepFor.tv_sec == 0 && sleepFor.tv_nsec == 0) || thrd_sleep(&sleepFor, &sleepRemaining) == 0)
  {
    // Get start time of the loop
    (void)timespec_get(&timeLoopStart, TIME_UTC);
    if (fetchCpuLoad(&procStat) == 0)
    {
      int pushErr = pushToSynchronizedQueue(ctx->outQueue, 0, procStat);

      if (pushErr != 0)
      {
        // Free the memory from fetch
        free(procStat);
        // In case of fail exit thread immidiately, but as we shutdown there is no error here
        thrd_exit(pushErr == PUSH_TO_SYNCHRONIZED_QUEUE_SHUTDOWN ? 0 : -1);
        return pushErr == PUSH_TO_SYNCHRONIZED_QUEUE_SHUTDOWN ? 0 : -1; // For compiler
      }
    }
    else
    {
      // In case of fail exit thread immidiately
      thrd_exit(-1);
      return -1; // For compiler
    }

    // Get end time of the loop
    (void)timespec_get(&timeLoopEnd, TIME_UTC);

    caclulateNextSleepLength(&sleepStep, &timeLoopStart, &timeLoopEnd, &sleepFor);
  }

  thrd_exit(0);
  return 0;
}
