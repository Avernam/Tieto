// Implemented API
#include "processAnalyzer/procStatDataAnalyzer.h"

// Local API
#include "processAnalyzer/analyzedData.h"
#include "processAnalyzer/procStatData.h"

// System API
#include <stdlib.h> // Defines malloc()
#include <string.h> // Defines memset()
#include <unistd.h> // Defines NULL

#define PRECISION         4      // Number of places after dot in calculations
#define PERCENT_MULTIPLER 100ULL // Multipler to reach percent values - assume 'unsigned long long'

// This structure corresponds to single normalized data row
// It's local-only structure used to simplify formula
typedef struct CpuLoadNormalized
{
  // This field holds data how much time we spent in user-space excluding Guest OS
  // userTime = ProcStatData.user - ProcStatData.guest
  unsigned long userTime;
  // This field holds data how much time we spent in user-space in low-priority excluding Guest OS
  // niceTime = ProcStatData.user - ProcStatData.guestNice
  unsigned long niceTime;
  // This field holds data how much time we spent idling
  // ProcStatData.idle + ProcStatData.ioWait
  unsigned long idleTime;
  // This field holds data how much time we spent for OS
  // ProcStatData.system + ProcStatData.irq + ProcStatData.softIrq
  unsigned long systemTime;
  // This field holds data how much time we spent for OS
  // userTime + niceTime + idleTime + systemTime + ProcStatData.guest + ProcStatData.guestNice + ProcStatData.steal
  unsigned long totalTime;
} CpuLoadNormalized;

static unsigned int powu(unsigned int base, unsigned short exp)
{
  unsigned int ret = 1;

  for (int i = 0; i < exp; i++)
  {
    ret = ret * base;
  }

  return ret;
}

int analyzeProcStatData(unsigned short precision, ProcStatData* procStatPrev, ProcStatData* procStatCurrent, AnalyzedData** data)
{
  // Return error if we get unexpected data
  if (procStatPrev == NULL || procStatPrev->cpuCount == 0 || procStatCurrent == NULL || procStatCurrent->cpuCount == 0 || data == NULL)
  {
    return ANALYZE_PROC_STAT_DATA_INVALID_ARGUMENT;
  }

  unsigned short normCpuCount = procStatPrev->cpuCount > procStatCurrent->cpuCount ? procStatCurrent->cpuCount : procStatPrev->cpuCount;

  // Allocate memory for output
  AnalyzedData* output = malloc(sizeof(AnalyzedData) + normCpuCount * sizeof(CpuLoadRelative));
  memset(output, 0, sizeof(AnalyzedData) + normCpuCount * sizeof(CpuLoadRelative));

  // Save normalized CPU count
  output->cpuCount = normCpuCount;

  // Currently assume constant precision of 2 places after dot
  output->precision = precision;

  // Calculate precision divisor - for 2 it's 10^2, for 0 it's 10^0, etc.
  unsigned int precisionDivisor = powu(10, output->precision);

  // Now let's point to the 'data rows' (treat aggregated/average just as additional CPU).
  // Our formula treats each 'data row' equally
  CpuLoad* dataRowPrev        = &procStatPrev->aggregated;
  CpuLoad* dataRowCurrent     = &procStatCurrent->aggregated;
  CpuLoadRelative* dataRowOut = &output->average;

  // Create processing buffer array
  // One for Previous state, one for Current state as we do not care to store anything more
  CpuLoadNormalized dataRowMid[2];

  for (unsigned short row = 0; row < normCpuCount + 1; row++)
  {
    // Used formulas are also visible in 'middle' structure definition comments, so it won't be listed and explained here

    // Firstly calculate for previous state
    dataRowMid[0].userTime   = dataRowPrev[row].user - dataRowPrev[row].guest;
    dataRowMid[0].niceTime   = dataRowPrev[row].nice - dataRowPrev[row].guestNice;
    dataRowMid[0].idleTime   = dataRowPrev[row].idle + dataRowPrev[row].ioWait;
    dataRowMid[0].systemTime = dataRowPrev[row].system + dataRowPrev[row].irq + dataRowPrev[row].softIrq;
    dataRowMid[0].totalTime  = dataRowMid[0].userTime + dataRowMid[0].niceTime + dataRowMid[0].idleTime + dataRowMid[0].systemTime
                              + dataRowPrev[row].guest + dataRowPrev[row].guestNice + dataRowPrev[row].steal;

    // Secondly calculate for current state
    dataRowMid[1].userTime   = dataRowCurrent[row].user - dataRowCurrent[row].guest;
    dataRowMid[1].niceTime   = dataRowCurrent[row].nice - dataRowCurrent[row].guestNice;
    dataRowMid[1].idleTime   = dataRowCurrent[row].idle + dataRowCurrent[row].ioWait;
    dataRowMid[1].systemTime = dataRowCurrent[row].system + dataRowCurrent[row].irq + dataRowCurrent[row].softIrq;
    dataRowMid[1].totalTime  = dataRowMid[1].userTime + dataRowMid[1].niceTime + dataRowMid[1].idleTime + dataRowMid[1].systemTime
                              + dataRowCurrent[row].guest + dataRowCurrent[row].guestNice + dataRowCurrent[row].steal;

    // Now get the deltas we need to get the output
    unsigned long userTimeDelta      = dataRowMid[1].userTime - dataRowMid[0].userTime;
    unsigned long niceTimeDelta      = dataRowMid[1].niceTime - dataRowMid[0].niceTime;
    unsigned long idleTimeDelta      = dataRowMid[1].idleTime - dataRowMid[0].idleTime;
    unsigned long systemTimeDelta    = dataRowMid[1].systemTime - dataRowMid[0].systemTime;
    unsigned long totalTimeDelta     = dataRowMid[1].totalTime - dataRowMid[0].totalTime;
    unsigned long guestTimeDelta     = dataRowCurrent[row].guest - dataRowPrev[row].guest;
    unsigned long guestNiceTimeDelta = dataRowCurrent[row].guestNice - dataRowPrev[row].guestNice;

    // Prevent divide by 0 in case if someone decided to make refresh rate near 0
    if (totalTimeDelta == 0)
    {
      // It's safe to continue as row is already initialized to 0
      continue;
    }

    // Calculate all direct fields - remember that we convert to percent with some precision
    dataRowOut[row].user      = (userTimeDelta * PERCENT_MULTIPLER * precisionDivisor) / totalTimeDelta;
    dataRowOut[row].nice      = (niceTimeDelta * PERCENT_MULTIPLER * precisionDivisor) / totalTimeDelta;
    dataRowOut[row].idle      = (idleTimeDelta * PERCENT_MULTIPLER * precisionDivisor) / totalTimeDelta;
    dataRowOut[row].system    = (systemTimeDelta * PERCENT_MULTIPLER * precisionDivisor) / totalTimeDelta;
    dataRowOut[row].guest     = (guestTimeDelta * PERCENT_MULTIPLER * precisionDivisor) / totalTimeDelta;
    dataRowOut[row].guestNice = (guestNiceTimeDelta * PERCENT_MULTIPLER * precisionDivisor) / totalTimeDelta;

    // Calculate extrapolated field
    // Overall in-use is (total - idle) / total
    dataRowOut[row].busy = ((totalTimeDelta - idleTimeDelta) * PERCENT_MULTIPLER * precisionDivisor) / totalTimeDelta;
  }

  // Return memory with calculation to the caller
  *data = output;

  // Return success
  return 0;
}
