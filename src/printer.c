// Implemented API
#include "processAnalyzer/printer.h"

// Local API
#include "processAnalyzer/analyzedData.h"      // Defines AnalyzedData
#include "processAnalyzer/synchronizedQueue.h" // Defines SynchronizedQueue operations

// System API
#include <stdio.h>   // Defines printf()
#include <stdlib.h>  // Defines free()
#include <threads.h> // Defines thrd_exit()

#define ASSUMED_PRECISION 2 // Hardcoded to 2 places after dot currently

static unsigned int powu(unsigned int base, unsigned short exp)
{
  unsigned int ret = 1;

  for (int i = 0; i < exp; i++)
  {
    ret = ret * base;
  }

  return ret;
}

int printer(void* context)
{
  // Extract context
  PrinterContext* ctx = (PrinterContext*)context;

  // Overwrite precision to hardcoded value
  ctx->precision = ASSUMED_PRECISION;

  // Prepare place to receive data from Reader
  unsigned short msgId = 0;
  AnalyzedData* data   = NULL;

  // Clear screen
  printf("\033[2J\n");
  printf("Waiting for printable data to arrive...\n");

  // While we can get information from analyzer to print the data
  while (popFromSynchronizedQueue(ctx->inQueue, &msgId, (void**)&data) == 0)
  {
    unsigned int precisionDivisor = powu(10, data->precision);

    // These are needed to align print precision with received data precision
    // i.e. if data has lower precision than expected we need to multiply for correct representation
    //      if data has higher precision than expected we need to divide for correct representation
    //      in any other case we do nothing
    unsigned int printPrecisionDivisor   = 1;
    unsigned int printPrecisionMultipler = 1;
    if (data->precision > ctx->precision)
    {
      printPrecisionDivisor = powu(10, data->precision - ctx->precision);
    }
    else
    {
      printPrecisionMultipler = powu(10, ctx->precision - data->precision);
    }
    // Clear screen
    printf("\033[2J\n");

    // Write out header
    printf("| CPU ID | Busy overall |     User     |     Nice     |    System    |     Guest    |  Niced guest |\n");
    printf("----------------------------------------------------------------------------------------------------\n");
    printf("|   Avg  |     %3lu.%02lu%%  |     %3lu.%02lu%%  |     %3lu.%02lu%%  |     %3lu.%02lu%%  |     %3lu.%02lu%%  |     "
           "%3lu.%02lu%%  |\n",
           data->average.busy / precisionDivisor,
           ((data->average.busy % precisionDivisor) * printPrecisionMultipler) / printPrecisionDivisor,
           data->average.user / precisionDivisor,
           ((data->average.user % precisionDivisor) * printPrecisionMultipler) / printPrecisionDivisor,
           data->average.nice / precisionDivisor,
           ((data->average.nice % precisionDivisor) * printPrecisionMultipler) / printPrecisionDivisor,
           data->average.system / precisionDivisor,
           ((data->average.system % precisionDivisor) * printPrecisionMultipler) / printPrecisionDivisor,
           data->average.guest / precisionDivisor,
           ((data->average.guest % precisionDivisor) * printPrecisionMultipler) / printPrecisionDivisor,
           data->average.guestNice / precisionDivisor,
           ((data->average.guestNice % precisionDivisor) * printPrecisionMultipler) / printPrecisionDivisor);

    for (unsigned short cpuNo = 0; cpuNo < data->cpuCount; cpuNo++)
    {
      printf("|   %3hu  |     %3lu.%02lu%%  |     %3lu.%02lu%%  |     %3lu.%02lu%%  |     %3lu.%02lu%%  |     %3lu.%02lu%%  |     "
             "%3lu.%02lu%%  |\n",
             cpuNo,
             data->cpu[cpuNo].busy / precisionDivisor,
             ((data->cpu[cpuNo].busy % precisionDivisor) * printPrecisionMultipler) / printPrecisionDivisor,
             data->cpu[cpuNo].user / precisionDivisor,
             ((data->cpu[cpuNo].user % precisionDivisor) * printPrecisionMultipler) / printPrecisionDivisor,
             data->cpu[cpuNo].nice / precisionDivisor,
             ((data->cpu[cpuNo].nice % precisionDivisor) * printPrecisionMultipler) / printPrecisionDivisor,
             data->cpu[cpuNo].system / precisionDivisor,
             ((data->cpu[cpuNo].system % precisionDivisor) * printPrecisionMultipler) / printPrecisionDivisor,
             data->cpu[cpuNo].guest / precisionDivisor,
             ((data->cpu[cpuNo].guest % precisionDivisor) * printPrecisionMultipler) / printPrecisionDivisor,
             data->cpu[cpuNo].guestNice / precisionDivisor,
             ((data->cpu[cpuNo].guestNice % precisionDivisor) * printPrecisionMultipler) / printPrecisionDivisor);
    }

    // Remember to release the memory
    free(data);
  }

  thrd_exit(0);
  return 0; // For compiler
}
